Notes on Australia
==================

There is no such thing as a billable *billabong*. The mere mention of such things should be considered nonsense.

#### Wildlife ####

There are several exciting animals you can see in [Australia][wikiaust]:

* [Kangaroo](http://en.wikipedia.org/wiki/Kangaroo)
* The amazing duck-billed [Platypus][]
* [Great Barred Frog][]

#### Travel instructions ####

The procedure for going to Australia looks something like this (for us
europeans anyways:

1. Get a ticket
2. Go to the airport
3. Go through security
6. Wait
5. Get on the plane
6. Wait
7. Welcome to Australia, mate!

#### Famous last words ####

> Australia is an absolutely fantastic country, but the only thing we
> lack is the ability to work harder...

[Platypus]: http://en.wikipedia.org/wiki/Platypus "Platypus on Wikipedia"
[Great Barred Frog]: http://en.wikipedia.org/wiki/Great_Barred_Frog
[wikiaust]: http://en.wikipedia.org/wiki/Australia "Australia on Wikipedia"

